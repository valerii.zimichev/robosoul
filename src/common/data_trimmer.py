from common.utils import *
import os

def run(file, folder, leng):
    start_time = time.time()        
    output(f'Launch python3 data_trimmer.py --file {file} --leng {leng}','start')
    
    dir = f'neuro{slash}data{slash}{leng}'
    if not os.path.exists(dir):
        os.mkdir(dir, mode=0o777)
    
    full_filename = f'{folder}{slash}{file}'
    origin = np.loadtxt(full_filename, 'float', delimiter = ',')
    
    if origin.ndim != 2:
        return
    
    if origin.shape[0] < leng + 1:
        return
        
    gap = [[x,x + leng] for x in range(1, origin.shape[0] - leng + 1)]
    
    for i in range(len(gap)):
        trim_filename = f'{dir}{slash}{i}_{file}'
        np.savetxt(trim_filename, origin[gap[i][0]:gap[i][1]], fmt='%.4f', delimiter=',')
        
    output(f'Created {len(gap)} trimmed matrixes from original {file}. Stored at {dir}.')        

    output('Session of data_trimmer.py ended in ', 'time', time.time()-start_time)  
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='String')    
    parser.add_argument('--file','-f', type = str, help='File to be trimmed. [default = ""]',default='')
    parser.add_argument('--leng','-l', type = int, help='Leng of trimming. [default = 2]', default=2)
    args = parser.parse_args()    
    prefix = args.prefix
    leng = args.leng
    
    run(file, leng)