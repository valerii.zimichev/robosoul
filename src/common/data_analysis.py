from common.utils import *
import pandas as pd
from openpyxl.chart import LineChart,Reference,Series
from openpyxl import load_workbook

def excel(netfolder, file):
    prefixes = os.listdir(netfolder)
    for prefix in prefixes:
        if prefix == '.gitkeep':
            continue
        data = {}
        iterations = os.listdir(f'{netfolder}{slash}{prefix}{slash}iteration')
        
        for num in range(len(iterations)):
            filename = f'{netfolder}{slash}{prefix}{slash}iteration{slash}{num}{slash}{file}'
            with open(filename, "r") as jsonfile: 
                results = json.load(jsonfile)              
            data.update({num:results})        

        df = pd.DataFrame(data).T
        lfilename = f'{netfolder}{slash}{prefix}{slash}lossfunction.xlsx'
        df.to_excel(lfilename, sheet_name = prefix)
        output(f'[Upd]{lfilename}')
        
        report = load_workbook(filename = lfilename)
        sheet = report.active
        sheet1 = report.get_sheet_by_name(prefix)
        chart = LineChart()
        values = Reference(sheet1, min_col = 10, min_row = 2, max_row = sheet1.max_row)
        series = Series(values)
        chart.series.append(series)

        chart.title = " Absolute value of loss function (prediction)"
        chart.x_axis.title = " Iteration "
        chart.y_axis.title = " Loss function "
        sheet1.add_chart(chart, "K2")    
        
        report.save(lfilename) 
        output(f'[Upd]{lfilename}')
        output('Charts plotted.')
        
def accuracy(netfolder, file):
    prefixes = os.listdir(netfolder)
    for prefix in prefixes:
        if prefix == '.gitkeep':
            continue
        count = 0
        succ_count = 0
        iterations = os.listdir(f'{netfolder}{slash}{prefix}{slash}iteration')
        
        for num in range(len(iterations)):
            filename = f'{netfolder}{slash}{prefix}{slash}iteration{slash}{num}{slash}{file}'
            with open(filename, "r") as jsonfile: 
                results = json.load(jsonfile)              
            if results['learning rate'] == 0:
                count += 1
                if results['prediction'] == results['experiment']:
                    succ_count += 1
        results = {
            'prefix': prefix,
            'success predictions': succ_count,
            'total': count,
            'accuracy, %': 100 * succ_count/count if count != 0 else None}
        afilename = f'{netfolder}{slash}{prefix}{slash}accuracy.json'
        with open(afilename, "w") as config_file:
            json.dump(results, config_file, indent=4)
        output(f'[Upd]{afilename}')

def spectrum():
    pass

def gif():
    pass