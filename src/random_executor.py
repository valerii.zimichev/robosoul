import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from common.utils import *
from exec import executor
from exec import matrix_inpreter
                                                
if __name__ == "__main__":       
    matrix_inpreter.run('walk','True')
    for i in range(int(os.environ['CI_NUMBER'])):
        executor.run()
        matrix_inpreter.run('random','False')