import os
from common.utils import *

def run(prefix, strategy, basesteps, foresight):
    start_time = time.time()
        
    output(f'Launch python3 weight_matrix_creator.py  --prefix {prefix} --strategy {strategy} --basesteps {basesteps} --foresight {foresight}','start')
    dir = f'neuro{slash}net{slash}{prefix}'
    os.mkdir(dir, mode=0o777)
             
    weights, biases, temperatures = [], [], []
    
    strat = [int(x,36) for x in strategy]
    strat[0] = basesteps * strat[0]
    strat[-1] = foresight * strat[-1]
    
    for j in range(len(strategy) - 1):
        a, b = strat[j], strat[j + 1]
        
        weight = np.random.randint(-50, 50, size = (a, b)) / 50
        w_name = f'{dir}{slash}weight_{j+1}.csv'
        np.savetxt(w_name, weight, fmt='%.4f', delimiter=',')
        output(f'[Upd]{w_name}')
        output(f'Created random weight matrix with shape: [{a}x{b}].')
            
        bias = np.zeros(shape=(1,b))
        b_name = f'{dir}{slash}bias_{j+1}.csv'
        np.savetxt(b_name,bias,fmt='%.4f',delimiter=',')
        output(f'[Upd]{b_name}')
        output(f'Created zero matrix of bias with shape: [1x{b}].')
                      
        weights.append(w_name)
        biases.append(b_name)

    with open(f'{dir}{slash}netconfig.json', "w") as config_file:
        CONFIG = {'prefix':prefix, 'strategy':strategy, 'arch':strat, 'basesteps': basesteps, 'foresight':foresight, 'layers':len(strategy)-1, 'weights':weights, 'biases':biases}
        json.dump(CONFIG, config_file, indent=4)
    
    output(f'[Upd]{dir}{slash}netconfig.json')
    output('Added new configurations.')
    output('Session of weight_matrix_creator.py ended in ', 'time', time.time()-start_time)  

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='String')    
    parser.add_argument('--prefix','-p', type = str, help='Input prefix of net. [default = ""]',default='')
    parser.add_argument('--strategy','-s', type = str, help='Architecture of layers. [default = 666]', default='666')
    parser.add_argument('--basesteps','-b', type = int, help='Basesteps is the value of steps can be source of one pass of net. [default = 2] ',default=2)
    parser.add_argument('--foresight','-f', type = int, help='Foresight is the value of steps can be predicted by one pass of net. [default = 1] ',default=1)
    args = parser.parse_args()    
    prefix = args.prefix
    strategy = args.strategy
    basesteps = args.basesteps
    foresight = args.foresight
    
    run(prefix, strategy, basesteps, foresight)
