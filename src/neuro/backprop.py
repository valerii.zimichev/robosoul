from common.utils import *

def run(prefix, learning, hyper, reference):        
    start_time = time.time()
    output(f'Launch python3 backprop.py --prefix {prefix} --learning {learning} --hyper {hyper} --reference {reference}','start')
    
    iterations_folder = f'neuro{slash}net{slash}{prefix}{slash}iteration'
    num = len(os.listdir(iterations_folder)) - 1
    dir = f'{iterations_folder}{slash}{num}'
    output(f'Iteration #{num}.')
    
    with open(f'neuro{slash}net{slash}{prefix}{slash}netconfig.json', "r") as config_file: 
        CONFIG = json.load(config_file)
    output(f'Found net with prefix: {CONFIG["prefix"]} strategy: {CONFIG["strategy"]} arch: {CONFIG["arch"]} basesteps: {CONFIG["basesteps"]} foresight: {CONFIG["foresight"]}','start')
    
    if (hyper > 0.9): 
        output('Influence of hyper parameter is very strong!','warning')

    with open(f'{dir}{slash}layers.json', "r") as config_file: 
        layers = json.load(config_file)   

    result = np.loadtxt(layers[-1], 'float', delimiter = ',')
    supervisor = np.loadtxt(reference, 'float', delimiter = ',') 
    supervisor = supervisor[-CONFIG['foresight']:]
    np.savetxt(f'{dir}{slash}reference.csv', supervisor, fmt = '%.4f', delimiter = ',')
    output(f'[Upd]{dir}{slash}reference.csv')    
    
    result = line(result)
    supervisor = line(supervisor)

    #output layer 
    #error
    delta = (result - supervisor) * result * (1 - result)
    np.savetxt(f'{dir}{slash}delta.csv', delta, fmt = '%.4f', delimiter = ',')
    weight = np.loadtxt(CONFIG['weights'][-1], 'float', delimiter = ',')
    bias = np.loadtxt(CONFIG['biases'][-1], 'float', delimiter = ',')
    layer = line(np.loadtxt(layers[-2], 'float', delimiter=','))
  
    #backpropagation of output layer
    weight = weight + learning * layer.T.dot(delta) - hyper * weight
    bias = bias + learning * delta
        
    np.savetxt(CONFIG['weights'][-1], weight, fmt='%.4f', delimiter=',')
    np.savetxt(CONFIG['biases'][-1], bias, fmt='%.4f', delimiter=',')  
    output(f'[Upd]{CONFIG["weights"][-1]}\n[Upd]{CONFIG["biases"][-1]}')
    #loss function    
    J = np.sum((supervisor-result)**2) + hyper * np.sum(weight**2)

    #backpropagation of hidden layers 
    for j in range(CONFIG['layers'] - 2, -1, -1):
        delta = (delta.dot(weight.T)) * layer * (1 - layer)
            
        weight = np.loadtxt(CONFIG['weights'][j], 'float', delimiter=',')
        bias = np.loadtxt(CONFIG['biases'][j], 'float', delimiter=',')

        layer =line(np.loadtxt(layers[j], 'float', delimiter=','))
            
        weight = weight + learning * layer.T.dot(delta) - hyper * weight
        bias = bias + learning * delta
        
        weight = high_pass_filter(weight, 0.005)
        
        np.savetxt(CONFIG['weights'][j], weight, fmt = '%.4f', delimiter = ',')
        np.savetxt(CONFIG['biases'][j], bias, fmt = '%.4f', delimiter = ',')  
        output(f'[Upd]{CONFIG["weights"][j]}\n[Upd]{CONFIG["biases"][j]}')
                          
        J += hyper * np.sum(weight**2)
 
    output('Updated matrixes of prediction according to backpropagation.')
    output(f'Loss function of {prefix} net: ' + '{:.4f}'.format(J)) 
    
    supervisor_ready = ready(*upscale_sensor_data(supervisor)[0][-6:])
    result_ready = ready(*upscale_sensor_data(result)[0][-6:])
    
    results = {
        'prefix': CONFIG["prefix"],
        'strategy': CONFIG["strategy"], 
        'basesteps': CONFIG["basesteps"],
        'foresight': CONFIG["foresight"],
        'learning rate': learning, 
        'hyper parameter': hyper, 
        'prediction': result_ready,
        'experiment': supervisor_ready,
        'loss function': J}
    with open(f'{dir}{slash}lossfunction.json', "w") as config_file:
        json.dump(results, config_file, indent=4)
    output(f'[Upd]{dir}{slash}lossfunction.json')    
    output('Session of backprop.py ended in ','time',time.time()-start_time) 
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='String')
    parser.add_argument('--prefix','-p', type = str, help='Input prefix of net. [default = ""]',default='')
    parser.add_argument('--learning','-l', type = float, help='Learning rate of backpropagation. [default = 0.1]',default = 0.1)
    parser.add_argument('--hyper','-hp', type = float, help='L2 normalization parameter. [default = 0]', default = 0)
    parser.add_argument('--reference','-r', type = float, help='Reference file. [default = ""]', default = '')    
    args = parser.parse_args()
    prefix = args.prefix
    learning = args.learning
    hyper = args.hyper
    reference = args.reference

    run(prefix, learning, hyper, reference)