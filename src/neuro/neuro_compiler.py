from common.utils import *
import shutil

def sigmoid(a):
    return 1.0/(1.0+np.exp(a))

def forward_pass(mtrx, w, b):
    return sigmoid(line(mtrx).dot(w) + line(b)).reshape(b.shape)
 

def run(prefix, source):
    start_time = time.time()
    
    output(f'Launch python3 neuro_compiler.py --prefix {prefix} --source {source}','start')
    
    iterations_folder = f'neuro{slash}net{slash}{prefix}{slash}iteration'
    if not os.path.exists(iterations_folder):
        os.mkdir(iterations_folder, mode=0o777)         

    num = len(os.listdir(iterations_folder))

    dir = f'{iterations_folder}{slash}{num}'
    if not os.path.exists(dir):
        os.mkdir(dir, mode=0o777)
        
    output(f'Iteration #{num}.')
    
    with open(f'neuro{slash}net{slash}{prefix}{slash}netconfig.json', "r") as config_file: 
        CONFIG = json.load(config_file)
    output(f'Found net with prefix: {CONFIG["prefix"]} strategy: {CONFIG["strategy"]} arch: {CONFIG["arch"]} basesteps: {CONFIG["basesteps"]} foresight: {CONFIG["foresight"]}','start')
    
    matrix = np.loadtxt(source, 'float', delimiter = ',')
    l_name = f'{dir}{slash}layer_0.csv'
    layers = [l_name]   
    with open(l_name, 'w') as file: 
        np.savetxt(file, matrix, fmt='%.4f', delimiter = ',')
        output(f'[Upd]{l_name}')         
    
    for j in range(CONFIG['layers']):
        weight = np.loadtxt(CONFIG['weights'][j], 'float', delimiter = ',')
        bias = np.loadtxt(CONFIG['biases'][j], 'float', delimiter = ',')
        
        matrix = forward_pass(matrix, weight, bias) 
        
        l_name = f'{dir}{slash}layer_{j+1}.csv'
        with open(l_name, 'w') as file: 
            np.savetxt(file, matrix, fmt='%.4f', delimiter = ',')
            output(f'[Upd]{l_name}')
            layers.append(l_name)
            
        w_name = f'{dir}{slash}weight_{j+1}.csv'
        with open(w_name, 'w') as file: 
            np.savetxt(file, weight, fmt='%.4f', delimiter = ',')
            output(f'[Upd]{w_name}')
            
        b_name = f'{dir}{slash}bias_{j+1}.csv'
        with open(b_name, 'w') as file: 
            np.savetxt(file, bias, fmt='%.4f', delimiter = ',')
            output(f'[Upd]{b_name}')
            
    with open(f'{dir}{slash}layers.json', "w") as config_file:
        json.dump(layers, config_file, indent=4)

    output('Session of neuro_compiler.py ended in ','time',time.time()-start_time) 
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='String')
    parser.add_argument('--prefix','-p', type = str, help='Input prefix of net. [default = ""]',default='')
    parser.add_argument('--source','-s', type = str, help='Sourcefile to be forwardpassed by prefix net. [default = ""]',default='')
    args = parser.parse_args()
    prefix = args.prefix
    source = args.source
    
    run(prefix, source)