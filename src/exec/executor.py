from common.utils import *
from uuid import uuid1
from telnetlib import Telnet
import shutil
import os

def listen(ser):
    ser.read_until(b'<',timeout=30)
    raw_acc = ser.read_until(b'\n')
    if raw_acc != b'':
        acc = [float(e) for e in raw_acc.split(b'\t')]
        output(f'accelerometer: {acc}')
    else:
        acc = False   #lost connection 
    return acc

def callback(ser,data):
    try:
        ser.write(data.encode())
        ser.read_until(b'>',timeout=5)
        buffer = '>' + ser.read_until(b'.').decode()
        if buffer == data:
            output(f'servo in: {data}')
        else:
            output('Something went wrong, your message was not sent or received correctly.','error')
            output(f'This message program sent: {data}\nThis message program received as sent: {buffer}')
    except ConnectionResetError:
        output('ConnectionResetError')
        ser.close()
        ser = serial_begin(port)
        callback(ser,data)
    return ser

def constructor(a=375,b=375,c=375,d=375,e=375,f=375,delay=3):
    data=f'>i:{a},{b},{c},{d},{e},{f},{"{:03d}".format(delay)}.'
    return data

def package_constructor(mtrx):
    data='>' 
    for a,b,c,d,e,f,delay in mtrx:
        data += f'i:{a},{b},{c},{d},{e},{f},{"{:03d}".format(delay)};'
    data = f'{data[:-1]}.'
    return data
    
def serial_begin(port):    
    try:
        ser = Telnet(port,8880)
        ser.read_until(b"Welcome",timeout=30)
    except TimeoutError:
        output('Lost connection.','error')
        output('Restarting connection')
        serial_begin(port)
    output(time.ctime()+f'\nconnected to: {port}:8880' ,'highlight')
    return ser

def save(acc):
    if len(acc) != 0: 
        data = np.array(acc)
        np.savetxt('sensor.csv',data,fmt='%.4f',delimiter=',')
        output('[Upd]sensor.csv')
        output('Sensor data recieved.')
    else: 
        output('No sensor data recieved.','warning')   

def executor(mtrx):
    #init connection
    instance = serial_begin(port)
    #am I standing?
    while True:
        #standpoint sent 
        instance = callback(instance, constructor())
        sensor = listen(instance)
        if type(sensor) == list: 
            if ready(*sensor): 
                break
            else:
                print('Master, lift me up, please...')
                time.sleep(1)
        else:
            instance = serial_begin(port)
    #ready to go!
    step = 0
    acc = []
    instance = callback(instance, package_constructor(mtrx))
    for i in mtrx: 
        sensor = listen(instance)
        #connection lost
        if sensor == False: 
            save(acc)
            output('Lost connection.', 'error')
            instance.close()
            return step, False          
        acc.append(sensor)
        step += 1
        #fallen on ground, connection stable
        if not ready(*sensor): 
            save(acc)
            output('I have fallen.', 'error')            
            instance = callback(instance, constructor())  
            instance.close()           
            return step, False
    save(acc)
    output('All matrix have been executed.') 
    instance = callback(instance, constructor())  
    instance.close()    
    return step, True

def run():
    start_time = time.time()
    output('Launch python3 executor.py', 'start')   
    
    matrix = np.loadtxt('matrix.csv', 'float', delimiter = ',')

    step, status = executor(matrix.astype(int))
    output(f'Steps to fall: {step}', 'highlight')    

    id = uuid1()
    shutil.copyfile('matrix.csv', f"{os.environ['OUT_DIR']}{slash}{id}.matrix.csv")
    output(f"[Upd]{os.environ['OUT_DIR']}{slash}{id}.matrix.csv")  
    shutil.copyfile('sensor.csv', f"{os.environ['OUT_DIR']}{slash}{id}.sensor.csv")
    output(f"[Upd]{os.environ['OUT_DIR']}{slash}{id}.sensor.csv")    
    output('Session of executor.py ended in ', 'time', time.time()-start_time) 
     
port = '192.168.50.79' 

if __name__ == "__main__":    
    run()
