from common.utils import *
from random import randrange

def interpreter(st,mtrx):
    '''gets input string and last value of servoin
        returns tuple of updated values servo
        if str='start' to initials
        if str='a=12 c=13' returns (12,90,13,90,90,90)
        if str='(12,90,13,90,90,90)' returns the same
        if str='(12,90,13,90)' returns the same
        if str='a=12 c=13 then e=50 then f=40 then start' compile the algorythm
        if str='a=5 c=3 then while a=6 b=2 and a=4 b=4' compile
        if str='a=5 c=3 then wait 3 then b=5' compile
    '''
    old=[*mtrx[-1]]
    if st == 'start':
        mtrx = np.vstack([mtrx,[375,375,375,375,375,375,3]])
        return mtrx
    if st == 'jackson':
        mtrx = interpreter('start then for 1 b=500 d=500 and a=500 c=500 and a=250 c=250 and b=250 d=250',mtrx)
        return mtrx
    if st == 'walk':
        mtrx = interpreter('for 1 e=360 g=1 and f=360 b=350 a=350 and a=330 and d=350 f=375 e=375 c=355 and e=395 f=400 b=400 a=375 d=375 and c=400 e=450 b=450 and b=490 d=470 a=480 c=480 and c=500 e=375 f=375 and e=350 f=350 d=350 b=375 a=250 c=370',mtrx)
        return mtrx
    
    if st == 'random':
        restrictions = np.loadtxt('exec/restrictions.txt', 'int', delimiter = '\t')   
        s = ''
        for i in range(15):
            s += ' then '
            for j in restrictions:
                s += f'{randrange(j[0],j[1],j[2])},'
            s = s[:-1]
        mtrx = interpreter(s,mtrx)
        return mtrx
    
    if st[0:5] == 'while':
        for i in range(100):
            for j in st[6:].split(' and '):
                mtrx=interpreter(j,mtrx)
    
    if st[0:3] == 'for':
        for l in range(int(st[4])): 
            for j in st[6:].split(' and '):
                mtrx=interpreter(j,mtrx)
        return mtrx
    
    try: #12,13,15
        q = list(int(i) for  i in st.split(','))
        for j in range(len(q),7): q.append(old[j])
        old = [q[k] for k in range(len(q))]
        mtrx = np.vstack([mtrx,old])
        return mtrx
    except ValueError:
        try: #a=12 b=13
            q = old
            tmp = list(st.split())
            for i in tmp: 
                q[ord(str(i).split('=')[0])-97]=int(str(i).split('=')[1])
            old = [q[k] for k in range(len(q))]
            mtrx = np.vstack([mtrx,old])
            return mtrx
        except IndexError: #a=12 b=13 then b=15
            for j in st.split(' then '):
                mtrx = interpreter(str(j),mtrx)
            return mtrx

def run(inp,reboot):
    start_time = time.time()    
    output(f'Launch python3 matrix_inpreter.py {inp} --reboot {reboot}','start')    
    matrix = np.array([[375,375,375,375,375,375,3]])
    matrix = interpreter(inp,[[*matrix[-1]]])
    np.savetxt('matrix.csv',matrix,fmt='%d',delimiter=',')
    output('[Upd]matrix.csv')
    output('Created matrix to be executed.')
    if reboot:
        with open("config.json", "w") as write_file:
            CONFIG = []
            json.dump(CONFIG, write_file,indent=4)
        output('[Upd]config.json')
        output('Created configuration file.')
    output('Session of matrix_inpreter.py ended in ','time',time.time()-start_time)  
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='String')
    parser.add_argument('inp', type = str, help = 'Input string to be interpreted')
    parser.add_argument('--reboot','-r', type = str2bool, help='Enable on-line learning. [default = True]',default=True)
    args = parser.parse_args()
    inp = args.inp
    reboot = args.reboot
    
    run(inp,reboot)
