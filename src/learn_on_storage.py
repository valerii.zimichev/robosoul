import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from common.utils import *
from neuro import weight_matrix_creator
from common import data_trimmer
from common import data_analysis
from neuro import neuro_compiler
from neuro import backprop

def pairing():
    output(f'Pairing data','start')
    for leng in os.listdir(f'neuro{slash}data'):
        if leng == '.gitkeep':
            continue
        allfiles = os.listdir(f'neuro{slash}data{slash}{leng}')
        for file1 in allfiles:
            if 'matrix.csv' in file1:
                if f'{file1.split(".matrix.csv")[0]}.sensor.csv' not in allfiles:
                    os.remove(f'neuro{slash}data{slash}{leng}{slash}{file1}')
            elif 'sensor.csv' in file1:
                if f'{file1.split(".sensor.csv")[0]}.matrix.csv' not in allfiles:
                    os.remove(f'neuro{slash}data{slash}{leng}{slash}{file1}')
    output(f'Pairing data - done','start')
                    
def normalizing():
    output(f'Normalizing data','start')
    for leng in os.listdir(f'neuro{slash}data'):
        if leng == '.gitkeep':
            continue
        allfiles = os.listdir(f'neuro{slash}data{slash}{leng}')
        for file1 in allfiles:
            file1 = f'neuro{slash}data{slash}{leng}{slash}{file1}'
            data = np.loadtxt(file1, 'float', delimiter = ',')
            if 'matrix.csv' in file1:
                norm = normalize_executor_matrix(data)
            elif 'sensor.csv' in file1:
                norm = normalize_sensor_data(data)
            if np.amax(norm) <= 1 and np.amin(norm) >= 0:
                with open(file1, 'w') as file: 
                    np.savetxt(file, norm, fmt='%.4f', delimiter = ',') 
            else:
                output(f'{file1} was not converted carefully','error')
    output(f'Normalizing data - done','start')

def splitting(validation):
    output(f'Splitting data','start')
    for leng in os.listdir(f'neuro{slash}data'):
        if leng == '.gitkeep':
            continue
        dir = f'neuro{slash}data{slash}{leng}'
        allfiles = os.listdir(dir)
        os.mkdir(f'{dir}_v', mode=0o777)
        validation_counter = int(len(allfiles) * validation)
        for file1 in allfiles[:validation_counter]:
            os.replace(f'{dir}{slash}{file1}', f'{dir}_v{slash}{file1}')
    output(f'Splitting data - done','start')
                  
if __name__ == "__main__":       
    parser = argparse.ArgumentParser(description='String')
    parser.add_argument('outdir', type = str, help='Output storage with learning data. [default = ""]',default='')
    parser.add_argument('--validation','-v', type = float, help='Output storage with learning data. [default = ""]',default=0.2)
    args = parser.parse_args()
    outdir = args.outdir
    validation = args.validation

    weight_matrix_creator.run(prefix = 'net1', strategy = '766', basesteps = 2, foresight = 1)
    weight_matrix_creator.run(prefix = 'net2', strategy = '7b6', basesteps = 2, foresight = 1)
    weight_matrix_creator.run(prefix = 'net3', strategy = '756', basesteps = 3, foresight = 1)
    weight_matrix_creator.run(prefix = 'net4', strategy = '756', basesteps = 3, foresight = 2)
    weight_matrix_creator.run(prefix = 'net5', strategy = '77777776', basesteps = 3, foresight = 1)
    
    for file in os.listdir(outdir):
        data_trimmer.run(file, folder = outdir, leng = 2)
        data_trimmer.run(file, folder = outdir, leng = 3)   
    
    pairing() #clean up data folder
    normalizing() #normalize data folder
    splitting(validation) #split leaning and validation data
    pairing() #clean up data folder
    
    #learning
    datafolder =  f'neuro{slash}data{slash}2'   
    for file in filter(lambda x: x.endswith('.matrix.csv'), os.listdir(datafolder)):
        neuro_compiler.run(prefix = 'net1', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net1', learning = 0.1, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
        neuro_compiler.run(prefix = 'net2', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net2', learning = 0.1, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
    
    datafolder = f'neuro{slash}data{slash}3'    
    for file in filter(lambda x: x.endswith('.matrix.csv'), os.listdir(datafolder)):
        neuro_compiler.run(prefix = 'net3', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net3', learning = 0.1, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
        neuro_compiler.run(prefix = 'net4', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net4', learning = 0.1, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
        neuro_compiler.run(prefix = 'net5', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net5', learning = 0.1, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
        
    #validation
    datafolder =  f'neuro{slash}data{slash}2_v'   
    for file in filter(lambda x: x.endswith('.matrix.csv'), os.listdir(datafolder)):
        neuro_compiler.run(prefix = 'net1', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net1', learning = 0, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
        neuro_compiler.run(prefix = 'net2', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net2', learning = 0, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
    
    datafolder = f'neuro{slash}data{slash}3_v'    
    for file in filter(lambda x: x.endswith('.matrix.csv'), os.listdir(datafolder)):
        neuro_compiler.run(prefix = 'net3', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net3', learning = 0, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
        neuro_compiler.run(prefix = 'net4', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net4', learning = 0, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
        neuro_compiler.run(prefix = 'net5', source = f'{datafolder}{slash}{file}')
        backprop.run(prefix = 'net5', learning = 0, hyper = 0, reference = f'{datafolder}{slash}{file.split(".matrix.csv")[0]}.sensor.csv')
        
    data_analysis.excel(netfolder = f'neuro{slash}net', file = 'lossfunction.json')
    data_analysis.accuracy(netfolder = f'neuro{slash}net', file = 'lossfunction.json')
    data_analysis.spectrum()
    data_analysis.gif()