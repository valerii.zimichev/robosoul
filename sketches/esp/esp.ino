#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>
#include <BMI160Gen.h>

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);
// wifi: ////////////////////////////////////////////////////////////////
bool debug = true;
const char *ssid = "iVal";  // Your ROUTER SSID
const char *pw = "Temperature43"; // and WiFi PASSWORD
// You must connect the phone to the same router,
// Then somehow find the IP that the ESP got from router, then:
// menu -> connect -> Internet(TCP) -> 192.168.0.102:8880  for UART0

/*************************  COM Port 0 *******************************/
#define UART_BAUD0 19200            // Baudrate UART0
#define SERIAL_PARAM0 SERIAL_8N1    // Data/Parity/Stop UART0
#define SERIAL0_TCP_PORT 8880       // Wifi Port UART0

#define bufferSize 1024
#define MAX_NMEA_CLIENTS 4
#define NUM_COM 1
#define DEBUG_COM 0 // debug output to COM1

WiFiServer server_0(SERIAL0_TCP_PORT);
WiFiServer *server[NUM_COM] = {&server_0};
WiFiClient TCPClient[NUM_COM][MAX_NMEA_CLIENTS];

int num = 0;

HardwareSerial* COM[NUM_COM] = {&Serial};

int16_t buf1[NUM_COM][bufferSize]; //useful buffer gotten from wifi
int16_t i1[NUM_COM] = {0};
int16_t buf2[bufferSize]; //buffer gotten from uart
int16_t i2[NUM_COM] = {0};
int16_t buf3[NUM_COM][bufferSize]; //full buffer gotten from wifi
int16_t i3[NUM_COM] = {0};
int16_t pointer[NUM_COM] = {0};

// servo: ////////////////////////////////////////////////////////////////
#define MAX_PULSE 600
#define MIN_PULSE 150

int8_t initial[7] = {8,4,0,10,3,7,0}; //bias of position
uint16_t ang[7];
uint16_t last[6] = {375,375,375,375,375,375}; //start point
uint16_t pos[6] = {375,375,375,375,375,375}; //current point
uint8_t servo_port[6] = {4,3,0,5,1,2}; //ports at pwm shield

struct Status_out {
    uint8_t status;
    String output;
};
//////////////////////////////////////////////////////////////////////////
Status_out servoin(String data)
{  
      Status_out out;
      if (data[0] != 'i' || data [1] != ':') {
          out.output = "[WARNING]Inappropriate input. " + data;
          out.status = 1;
          return out;}
      if (data.length() < 30 ) {
          out.output = "[WARNING]Inappropriate length. " + data; 
          out.status = 1;
          return out;}          
      for (byte i = 2; i < 29; i++)  {
          char smb = data[i];
          if ((static_cast<int>(smb) < 48) || (static_cast<int>(smb) > 57)) {
                data = "] at " + data; 
                data = smb + data;
                data = "[WARNING]Inappropriate symbol found. [" + data;
                out.output = data;
                out.status = 1;
                return out;}
          if  (i % 4 == 0) i += 1;
      }//for symbol warning
      for (byte i = 0; i < 7; i++) {
          ang[i] = strtol(&data.substring(4*i+2, 4*i+5)[0], NULL, 10) + initial[i]; //getting ang[]
      }//for reading      
      
      for (byte i = 0; i < 6; i++) {
          if ((ang[i] > MAX_PULSE) || (ang[i] < MIN_PULSE)) {
                data = "] at " + data;
                data = ang[i] + data;
                data = "[WARNING]Inappropriate angle found. [" + data;
                out.output = data;
                out.status = 1;
                return out;
          }
          out.output += "input["; 
          out.output += i;
          out.output += "]:\t";
          out.output += ang[i];
          out.output += "\r\n"; 
      }//for angle warning      
//moving from last[] to ang[]
      while(pos[0] != ang[0] || pos[1] != ang[1] || pos[2] != ang[2] || pos[3] != ang[3] || pos[4] != ang[4] || pos[5] != ang[5]) {
          for (byte i = 0; i < 6; i++) {
              if (last[i] == ang[i]) continue;
              if (pos[i] == ang[i]) continue;
              if (pos[i] - ang[i] > 0) pos[i] -= 1;
              else pos[i] += 1;
              pwm.setPWM(servo_port[i], 0, pos[i]);
              delay(ang[6]);
          }//for index      
      }//while eq
      for (byte i = 0; i < 6; i++) last[i] = pos[i];
      out.status = 0;
      return out;
}//servoin

Status_out get_accel()
{
    sensors_event_t event; 
    accel.getEvent(&event);
    Status_out out;
    out.output = "<"; 
    out.output += event.acceleration.x;
    out.output += "\t";
    out.output += event.acceleration.y;
    out.output += "\t";
    out.output += event.acceleration.z;
    out.status = 0;
    return out;
}

float convertRawGyro(int gRaw) {
  // since we are using 250 degrees/seconds range
  // -250 maps to a raw value of -32768
  // +250 maps to a raw value of 32767
  float g = (gRaw * 250.0) / 32768.0;
  return g;
}

Status_out get_gyro()
{
    int gxRaw, gyRaw, gzRaw;
    float gx, gy, gz;  
    BMI160.readGyro(gxRaw, gyRaw, gzRaw);
    Status_out out;
    out.output = "\t"; 
    out.output += convertRawGyro(gxRaw);
    out.output += "\t";
    out.output += convertRawGyro(gyRaw);
    out.output += "\t";
    out.output += convertRawGyro(gzRaw);
    out.status = 0;
    return out;
}

void setup() {
      pinMode(LED_BUILTIN, OUTPUT);
      COM[0]->begin(UART_BAUD0, SERIAL_PARAM0, SERIAL_FULL);

      WiFi.mode(WIFI_STA);
      WiFi.begin(ssid, pw);
      if (debug) COM[DEBUG_COM]->print("Connect to Wireless network: ");
      if (debug) COM[DEBUG_COM]->println(ssid);
      while (WiFi.status() != WL_CONNECTED) {
            delay(500);
            if (debug) COM[DEBUG_COM]->print(".");
      }
      if (debug) COM[DEBUG_COM]->print("\nWiFi connected - done\nIP address: ");      
      if (debug) COM[DEBUG_COM]->print(WiFi.localIP());    
      if (debug) COM[DEBUG_COM]->print(":");
      if (debug) COM[DEBUG_COM]->println(SERIAL0_TCP_PORT);  
      
      server[0]->begin(); // start TCP server
      server[0]->setNoDelay(true);
      if (debug) COM[DEBUG_COM]->println("Start TCP Server - done");
      
    
      pwm.begin();
      pwm.setPWMFreq(60);  // This is the maximum PWM frequency
      servoin("i:375,375,375,375,375,375,005.");  
      if (debug) COM[DEBUG_COM]->println("Start PWM servo - done");
      
      if(!accel.begin()) {
          if (debug) COM[DEBUG_COM]->println("Ooops, no ADXL345 detected ... Check your wiring!");
          while(1);
      }
      accel.setRange(ADXL345_RANGE_2_G);
      sensors_event_t event; 
      accel.getEvent(&event);
      if (debug) COM[DEBUG_COM]->println("Start accelerometer - done");

      BMI160.begin(BMI160GenClass::I2C_MODE);
      uint8_t dev_id = BMI160.getDeviceID();
      BMI160.setGyroRange(250);
      if (debug) COM[DEBUG_COM]->println("Start gyroscope - done");
}

void loop()
{
// new client:  ///////////////////////////////////////////////////////////
    for (int j = 0; j < NUM_COM ; j++)
    {
      if (server[num]->hasClient())
      {
        for (byte i = 0; i < MAX_NMEA_CLIENTS; i++) {
          //find free/disconnected spot
            if (!TCPClient[num][i] || !TCPClient[num][i].connected()) {
                if (TCPClient[num][i]) TCPClient[num][i].stop();
                TCPClient[num][i] = server[num]->available();
                if (debug) COM[DEBUG_COM]->print("New client for COM");
                if (debug) COM[DEBUG_COM]->print(j);
                if (debug) COM[DEBUG_COM]->print('/');
                if (debug) COM[DEBUG_COM]->println(i);
                if(TCPClient[num][i]) TCPClient[num][i].println("Welcome");
                continue;
            }
        }
        //no free/disconnected spot so reject
        WiFiClient TmpserverClient = server[j]->available();
        TmpserverClient.stop();
      }
    }
//////////////////////////////////////////////////////////////////////////
    for (byte cln = 0; cln < MAX_NMEA_CLIENTS; cln++) {        
        if (TCPClient[num][cln]) {
            String buffer;
            while (TCPClient[num][cln].available()) {              
                buf3[num][i3[num]] = TCPClient[num][cln].read(); // read char from wifi
                buffer += char(buf3[num][i3[num]]);
                if (debug) COM[DEBUG_COM]->println(i3[num]);
                if (i3[num] < bufferSize) {
                    i3[num]++;  
                } else {
                    if (debug) COM[DEBUG_COM]->println("Buffer stack overflow.");
                }
            }
            TCPClient[num][cln].print(buffer);
            if (i3[num] > 0 && buf3[num][0] == '>') {
                for (i1[num] = 0; i1[num] < i3[num]; i1[num]++) buf1[num][i1[num]] = buf3[num][i1[num]];
                pointer[num] = 1;
                i3[num] = 0;
            }//if buf3 
        }//if available
    }//for clients    
    if (pointer[num] + 29 < i1[num]) { 
        String toUART;
        String toWifi = "\r\nJust sent to servo. ";
        for (byte sym = 0; sym < 30; sym++) {
            toUART += char(buf1[num][pointer[num]+sym]);
        }
        Status_out servo = servoin(toUART);
        for(byte cln = 0; cln < MAX_NMEA_CLIENTS; cln++) {   
            if(TCPClient[num][cln]) {
                COM[num]->println(toUART); // now send to UART(num):                     
                TCPClient[num][cln].println(toWifi + toUART + "\r\n" + servo.output);
            }//if
        }//for
        pointer[num] += 30;      
        if (servo.status == 0) {      
            toWifi = get_accel().output + get_gyro().output;
            for(byte cln = 0; cln < MAX_NMEA_CLIENTS; cln++) {   
                if(TCPClient[num][cln]) {
                    COM[num]->println(toWifi); // now send to UART(num):                     
                    TCPClient[num][cln].println(toWifi);
                }
            }
        }      
    } else if (i1[num] > pointer[num]) {
        i3[num] = i1[num] - pointer[num];
        for (byte sym = 0; sym < i3[num]; sym++) buf3[num][sym] = buf1[num][pointer[num]+sym];
        i1[num] = 0; 
        pointer[num] = 0;                   
    } else {
        i1[num] = 0;
        pointer[num] = 0; 
    }
    if (i3[num] > 4 && i3[num] != 21) { //'\n' ignored       
        String toWifi = "[WARNING]Not sent. ";
        for (byte sym = 0; sym < i3[num]; sym++) toWifi += (char)buf3[num][sym];
        toWifi += ("\r\nStart commandline with '>'. Example: '>i:375,375,375,375,375,375,005.'.");
        for(byte cln = 0; cln < MAX_NMEA_CLIENTS; cln++) {
            if(TCPClient[num][cln]) TCPClient[num][cln].println(toWifi);
        }    
    } else if (i3[num] == 21) {
        for(byte cln = 0; cln < MAX_NMEA_CLIENTS; cln++) {   
            if(TCPClient[num][cln]) TCPClient[num][cln].println("Received twenty-one welcome symbols");
        }
    }    
    if (COM[num]->available()) {
        String myString[NUM_COM];
        while (COM[num]->available()) {
            myString[num] += (char)Serial.read(); // read char from UART(num)
        }
        for(byte cln = 0; cln < MAX_NMEA_CLIENTS; cln++) {   
            if(TCPClient[num][cln]) TCPClient[num][cln].print(myString[num]);// now send to WiFi:
        }        
    }
    i3[num] = 0;
}
